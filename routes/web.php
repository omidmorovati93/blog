<?php

use App\Http\Controllers\Admin\PostsController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[PostsController::class,'index'])->middleware('auth');

Route::get('login', [LoginController::class, 'view'])->name('login.view');
Route::post('login', [LoginController::class, 'login'])->name('login.check');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::resource('posts', PostsController::class);
});
