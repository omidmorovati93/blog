var app = document.getElementsByTagName("HTML")[0];
var button= document.getElementById('theme-switch');
function toggle_light_mode() {
    if (localStorage.lightMode == "dark") {
        localStorage.lightMode = "light";
        app.classList.remove('dark-skin');
        button.classList.add('fa-sun');
        button.classList.remove('fa-moon');
    } else {
        localStorage.lightMode = "dark";
        app.classList.add('dark-skin');
        button.classList.remove('fa-sun');
        button.classList.add('fa-moon');
    }
}


if (localStorage.lightMode == "dark") {
    app.classList.add('dark-skin');
    button.classList.add('fa-moon');
    button.classList.remove('fa-sun');
}
