
# Simple Blog

## Requirements
PHP v7.4 and above.
## Installation

copy and change .env by your server configuration

```bash
cp .env.example env
```   
install packages :

```bash
composer install
```
create tables :

```bash
php artisan migrate
```   
run seeders to add default users and posts :
 ```bash
 php artisan db:seed
```

## Login as Administrator

by running php artisan db:seed you can login as admin with this credentials:

email : admin@admin.com

password : admin123

## Author

- [@omidmorovati93](https://gitlab.com/omidmorovati93)
