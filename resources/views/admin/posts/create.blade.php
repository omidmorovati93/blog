@extends('layouts.admin')
@section('content')
    <div class="card-title">
        <h4>ایجاد مطلب</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="basic-form p-10">
                    @include('partials.errors')
                    @include('partials.success')
                    <form method="post" enctype="multipart/form-data" action="{{ route('posts.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="title">عنوان مطلب</label>
                            <input id="title" name="title" type="text"
                                   class="form-control input-default hasPersianPlaceHolder"
                            >
                        </div>
                        <div class="form-group">
                            <label for="content">محتوا</label>
                            <input id="content" name="content" type="text"
                                   class="form-control input-default hasPersianPlaceHolder"
                            >
                        </div>

                        <div class="form-group">
                            <label for="file">تصویر مطلب</label>
                            <input type="file" id="img" name="img">
                        </div>
                        <div class="form-group m-t-20">
                            <button type="submit" class="btn btn-primary m-b-10 m-l-5">ثبت اطلاعات
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
