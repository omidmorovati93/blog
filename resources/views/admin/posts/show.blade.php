@extends('layouts.admin')
@section('content')
    <div class="card-title">
        <h4>مشاهده مطلب</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="basic-form p-10">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">عنوان مطلب</label>
                            <input id="title" name="title" type="text"
                                   class="form-control input-default hasPersianPlaceHolder"
                                   value="{{ $post->title  }}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="content">محتوا</label>
                            <input id="content" name="content" type="text"
                                   class="form-control input-default hasPersianPlaceHolder"
                                   value="{{$post->content }}"
                            >
                        </div>

                        <div class="form-group">
                            <label for="logo">تصویر مطلب</label>
                            @if(isset($post->thumbnail))
                                <img src="{{$post->thumbnail}}" alt="تصویر" height="100" width="100">
                            @else ---
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
