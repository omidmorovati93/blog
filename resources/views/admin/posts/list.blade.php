@extends('layouts.admin')
@section('content')
    <div class="card-title">
        <h4>لیست مطالب </h4>
    </div>
    <div class="card-body">
        @include('partials.success')
        @include('partials.errors')
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>نویسنده</th>
                    <th>عنوان</th>
                    <th> عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <th scope="row">{{ $post->id }}</th>
                        <td>{{ $post->author->name }}</td>
                        <td>{{ $post->title }}</td>
                        <td>

                            <a href="{{ route('posts.show',[$post->id]) }}">
                                <i class="fa fa-eye"></i>
                            </a>

                            @can('update', App\Models\Post::class)
                                <a href="{{ route('posts.edit',[$post->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                            @endcan
                            @can('delete', App\Models\Post::class)
                                <x-delete route="posts.destroy">{{$post->id}}</x-delete>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
