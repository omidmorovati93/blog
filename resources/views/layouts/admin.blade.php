<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">
    <title>blog</title>
    <link href="/css/lib/bootstrap/bootstrap-rtl.min.css" rel="stylesheet">
    <link href="/css/helper.css" rel="stylesheet">
    <link href="/css/fonts.css" rel="stylesheet">
    <link href="/css/admin/style.css" rel="stylesheet">
    <!--CK editor -->
    <link href="/css/lib/ckeditor/plugins/contents.css" rel="stylesheet">
    <!--DataTable -->
    {{--    <link rel="stylesheet" type="text/css" href="/datatables/css/datatables.min.css">--}}
    <link rel="stylesheet" type="text/css" href="/datatables/css/jquery.dataTables.min2.css">
    <link rel="stylesheet" type="text/css" href="/datatables/css/select.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="/datatables/css/dataTables.checkboxes.css">
    <!--select2 -->
    <link href="/js/lib/select2/select2.min.css" rel="stylesheet"/>
    <!--font awesome-->
    <link href="/fontawesome/css/all.css" rel="stylesheet"/>
    <!--modal image-->
    <link href="/css/lib/modal-image.css" rel="stylesheet"/>
    <!--Persian Date Picker-->
    <link href="/css/lib/persianDatePicker/persian-datepicker.min.css" rel="stylesheet"/>
    <style>
        ul {
        }

        li {
        }

        ul li > ul {
            margin-right: 10px;
        }
    </style>
</head>
<body>
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- Main wrapper  -->
<div id="main-wrapper">
    <!-- header header  -->
    <div class="header">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- Logo -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/">
                    <!-- Logo icon -->
                    <b><img src="/images/logo.png" alt="homepage" class="dark-logo"/></b>
                    <!--End Logo icon -->
                    <!-- Logo text -->
                    <span>وبلاگ</span>
                </a>
            </div>
            <!-- End Logo -->
            <div class="navbar-collapse">
                <!-- toggle and nav items -->

                <!-- User profile and search -->

            </div>
            <ul class="dropdown-user">
                <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> خروج</a></li>
            </ul>
        </nav>
    </div>
    <!-- End header header -->
    <!-- Left Sidebar  -->
    <div class="right-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="nav-devider"></li>
                    <li class="nav-label"></li>


                    <li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-book"></i><span
                                class="hide-menu">مطالب
                        </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{route('posts.index')}}">لیست مطالب</a></li>
                            @can('create', App\Models\Post::class)
                                <li><a href="{{route('posts.create')}}">مطلب جدید</a></li>
                            @endcan
                        </ul>
                    </li>

                </ul>

            </nav>

            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </div>
    <!-- End Left Sidebar  -->
    <!-- Page wrapper  -->
    <div class="page-wrapper">

        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<script src="/js/lib/jquery/jquery.min.js"></script>

<!-- Bootstrap tether Core JavaScript -->
<script src="/js/lib/bootstrap/js/popper.min.js"></script>
<script src="/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="/js/jquery.slimscroll.js"></script>
<!--Menu sidebar -->
<script src="/js/sidebarmenu.js"></script>
<!--CK editor -->
<script src="/js/lib/ckeditor/ckeditor.js"></script>

<!--stickey kit -->
<script src="/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="/js/lib/select2/select2.min.js"></script>
<!--Custom JavaScript -->
<script src="/js/custom.min.js"></script>
<!--DataTables -->
{{--<script type="text/javascript" charset="utf8" src="/datatables/js/datatables.min.js"></script>--}}
<script type="text/javascript" charset="utf8" src="/datatables/js/jquery.dataTables.min2.js"></script>
<script type="text/javascript" charset="utf8" src="/datatables/js/dataTables.select.min.js"></script>
<script type="text/javascript" charset="utf8" src="/datatables/js/dataTables.checkboxes.min.js"></script>
<!--Create Jquery Post Request -->
<script src="/js/lib/jquery-postback/jquery-postback.js"></script>
<!--Persian Date Picker -->
<script src="/js/lib/persianDatePicker/persian-date.min.js"></script>
<script src="/js/lib/persianDatePicker/persian-datepicker.min.js"></script>
@stack('scripts')

</body>
</html>
