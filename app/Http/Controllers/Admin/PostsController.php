<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CreateRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Post;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(): View
    {
        $posts = Post::with('author')->get();
        return view('admin.posts.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(): View
    {
        $this->authorize(Post::class,'create');
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request): RedirectResponse
    {
        $this->authorize(Post::class,'create');
        $post = Post::create([
            'user_id' => Auth::id(),
            'title'   => $request->input('title'),
            'content' => $request->input('content'),
        ]);
        $post->addMedia($request->file('img'))
            ->preservingOriginal()
            ->toMediaCollection();
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function show(int $id): View
    {
        $post = Post::with('media')->findOrFail($id);
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $this->authorize(Post::class,'update');
        $post = Post::with('media')->findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, int $id): RedirectResponse
    {
        $this->authorize(Post::class,'update');
        $post = Post::findOrFail($id);
        $post->fill($request->validated())->save();
        if ($request->has('img')) {
            $post->addMedia($request->file('img'))
                ->preservingOriginal()
                ->toMediaCollection();
        }
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        $this->authorize(Post::class,'delete');
        $post = Post::findOrFail($id);
        $post->delete();
        return \Illuminate\Support\Facades\Response::make('مطلب با موفقیت حذف گردید');
    }
}
