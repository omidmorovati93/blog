<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'   => User::inRandomOrder()->first()->id ?? User::factory(),
            'title'     => $this->faker->word,
            'content'   => $this->faker->sentence(),
            'thumbnail' => $this->faker->url
        ];
    }
}
