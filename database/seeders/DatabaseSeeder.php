<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'super-admin',
            'email'    => 'admin@admin.com',
            'password' => 'admin123'
        ]);
        \App\Models\User::factory(10)->create();
        \App\Models\Post::factory(10)->create();
    }
}
